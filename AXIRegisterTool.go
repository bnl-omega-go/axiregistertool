//go:build linux || darwin
// +build linux darwin

package main

import (
	"flag"
	"log"

	"gitlab.cern.ch/bnl-omega-go/axi"
)

func main() {

	base := flag.Uint64("b", 0x0, "Base address of the AXI register table (ex: 0x3000)")
	offset := flag.Uint64("o", 0x0, "Register offset, in bytes (ex: 0xFF)")
	value := flag.Int64("v", -1, "32 bit value to write in the register (ex: 0x0000ABCD), ommit or use -1 if reading only")

	flag.Parse()

	registers := map[string][]int64{"REG": []int64{int64(*base), int64(*offset), int64(axi.AXIRW)}}

	ctrl := axi.AXIController{}
	ctrl.BookRegisters(registers)

	if *value != -1 {
		log.Printf("Writing %x to register with base %x and offset %x", *value, *base, *offset)
		ctrl.Write("REG", uint32(*value))
	}

	valueread := ctrl.Read("REG")
	log.Printf("Read value of register with base %x and offset %x : %x", *base, *offset, valueread)
}
